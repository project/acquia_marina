The master branch is no longer in use.  Please clone the 6.x or 7.x branch.

Example:
git clone --branch 7.x-1.x aquariumtap@git.drupal.org:project/acquia_marina.git